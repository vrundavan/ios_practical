//
//  Home_Cell.swift
//  ObviousPractical
//
//  Created by Dipak on 28/11/22.
//

import UIKit

class Home_Cell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgGrid: UIImageView!
    @IBOutlet weak var viewBack: viewEX!
}
