//
//  Details_Cell.swift
//  ObviousPractical
//
//  Created by Dipak on 28/11/22.
//

import UIKit

class Details_Cell: UICollectionViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgNasa: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}
