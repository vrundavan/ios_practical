//
//  BaseVC.swift
//  ObviousPractical
//
//  Created by Dipak on 28/11/22.
//

import UIKit

class BaseVC: UIViewController, UINavigationBarDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    func popVC(isAnimation: Bool){
        self.navigationController?.popViewController(animated: isAnimation)
    }

}
