//
//  ViewController.swift
//  ObviousPractical
//
//  Created by Dipak on 28/11/22.
//

import UIKit
import SDWebImage
import Alamofire

class HomeVC: BaseVC {

    //Outlet
    @IBOutlet weak var CollGrid: UICollectionView!
    
    //Variable Declaratiob
    var arrHome = [HomeModel]()
    
    //View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if NetworkReachabilityManager()!.isReachable{
            nasaPictures()
        }else{
            showToast("Please check internet connection.")
        }
    }
}


//MARK: Api Call
extension HomeVC {
    func nasaPictures() {
        let apiName = getFinalApi()
        ServiceManager.callAPI(url: apiName, parameter: nil , isShowLoader: true) { (result) in
            if let dictData = result as? [[String: Any]]
            {
                self.arrHome = [HomeModel]()
                for (_, element) in dictData.enumerated() {
                    self.arrHome.append(HomeModel(fromDictionary: element))
                }
                self.arrHome = self.arrHome.sorted(by: {$0.date < $1.date})
                self.reloadData()
            }
        } failure: { (error) in
            HIDE_CUSTOM_LOADER()
            showToast(error)
        } connectionFailed: { (error) in
            HIDE_CUSTOM_LOADER()
            showToast(error)
        }
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.CollGrid.reloadData()
        }
    }
}

//MARK: Collectionview Delegate and Datasurce
extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrHome.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = (self.CollGrid.frame.width) / 2
        if UIDevice.current.userInterfaceIdiom == .pad{
            width = (self.CollGrid.frame.width) / 3
        }
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : Home_Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Home_Cell", for: indexPath) as! Home_Cell
        if arrHome.count > 0 {
            if let dict = arrHome[indexPath.row] as? HomeModel{
                cell.imgGrid.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imgGrid.sd_setImage(with: URL(string: dict.url ?? defaultImg), placeholderImage: UIImage(named: "placeholder"))
                cell.lblName.text = dict.title
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeDetailsVC") as! HomeDetailsVC
        detailsVC.arrDetails = self.arrHome
        detailsVC.selectIndex = indexPath.row
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}
