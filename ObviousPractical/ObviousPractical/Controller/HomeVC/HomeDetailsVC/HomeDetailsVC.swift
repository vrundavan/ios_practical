//
//  HomeDetailsVC.swift
//  ObviousPractical
//
//  Created by Dipak on 28/11/22.
//

import UIKit
import SDWebImage

class HomeDetailsVC: BaseVC {

    //Outlet
    @IBOutlet weak var CollDetails: UICollectionView!
    
    //Variable Declaration
    var arrDetails = [HomeModel]()
    var selectIndex = 0
    
    //View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.CollDetails.delegate = self
        self.CollDetails.dataSource = self
        self.CollDetails.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CollDetails.performBatchUpdates(nil) { _ in
            self.CollDetails.scrollToItem(at:IndexPath(item: self.selectIndex, section: 0), at: .right, animated: false)
            self.CollDetails.layoutSubviews()
        }
    }
    
    //Back button action
    @IBAction func btnBackAction(_ sender: UIButton) {
        popVC(isAnimation: true)
    }
}

//MARK: Collectionview Delegate and Datasurce
extension HomeDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.CollDetails.frame.width, height: self.CollDetails.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : Details_Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Details_Cell", for: indexPath) as! Details_Cell
        if arrDetails.count > 0 {
            if let dict = arrDetails[indexPath.row] as? HomeModel{
                cell.imgNasa.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imgNasa.sd_setImage(with: URL(string: dict.url ?? defaultImg), placeholderImage: UIImage(named: "placeholder"))
                cell.lblTitle.text = dict.title
                cell.lblDescription.text = dict.explanation
                cell.lblDate.text = dict.date
            }
        }
        return cell
    }
}
