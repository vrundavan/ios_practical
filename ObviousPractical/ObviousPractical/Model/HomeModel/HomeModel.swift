//
//  HomeModel.swift
//  ObviousPractical
//
//  Created by Dipak on 28/11/22.
//

import Foundation


class HomeModel : NSObject, NSCoding{

    var copyright : String!
    var date : String!
    var explanation : String!
    var hdurl : String!
    var mediaType : String!
    var serviceVersion : String!
    var title : String!
    var url : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        copyright = dictionary["copyright"] as? String
        date = dictionary["date"] as? String
        explanation = dictionary["explanation"] as? String
        hdurl = dictionary["hdurl"] as? String
        mediaType = dictionary["media_type"] as? String
        serviceVersion = dictionary["service_version"] as? String
        title = dictionary["title"] as? String
        url = dictionary["url"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if copyright != nil{
            dictionary["copyright"] = copyright
        }
        if date != nil{
            dictionary["date"] = date
        }
        if explanation != nil{
            dictionary["explanation"] = explanation
        }
        if hdurl != nil{
            dictionary["hdurl"] = hdurl
        }
        if mediaType != nil{
            dictionary["media_type"] = mediaType
        }
        if serviceVersion != nil{
            dictionary["service_version"] = serviceVersion
        }
        if title != nil{
            dictionary["title"] = title
        }
        if url != nil{
            dictionary["url"] = url
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         copyright = aDecoder.decodeObject(forKey: "copyright") as? String
         date = aDecoder.decodeObject(forKey: "date") as? String
         explanation = aDecoder.decodeObject(forKey: "explanation") as? String
         hdurl = aDecoder.decodeObject(forKey: "hdurl") as? String
         mediaType = aDecoder.decodeObject(forKey: "media_type") as? String
         serviceVersion = aDecoder.decodeObject(forKey: "service_version") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if copyright != nil{
            aCoder.encode(copyright, forKey: "copyright")
        }
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if explanation != nil{
            aCoder.encode(explanation, forKey: "explanation")
        }
        if hdurl != nil{
            aCoder.encode(hdurl, forKey: "hdurl")
        }
        if mediaType != nil{
            aCoder.encode(mediaType, forKey: "media_type")
        }
        if serviceVersion != nil{
            aCoder.encode(serviceVersion, forKey: "service_version")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }

    }

}
